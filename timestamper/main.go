package main

import (
    "os"
    "fmt"

    "bufio"
    "time"
)

func main() {
    
    file, err := os.Create("time.stamp")
    if err != nil {
        fmt.Println("stamp already exists")
        
    }
    defer file.Close()
    scanner := bufio.NewScanner(os.Stdin)
    for scanner.Scan() {
        note := scanner.Text()
        file.WriteString(note+"\n")
        value := time.Now()
        fmt.Println("Timestamp is "+value.String()+"\n")
        file.WriteString(value.String())
    
    }
}
