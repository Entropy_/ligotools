#!/bin/bash

mate-terminal --geometry=43x26+0+0 --profile='silverpaws' -e 'hd '$1
mate-terminal --geometry=50x40+200+0 --profile='' -e 'hd '$1
mate-terminal --geometry=50x50+400+0 --profile='silverpaws' -e 'hd '$1
mate-terminal --geometry=50x43+200+50 --profile='ed:epic' -e 'hd '$1
mate-terminal --geometry=50x23+600+50 --profile='greeon' -e 'hd '$1
mate-terminal --geometry=50x10+400+50
